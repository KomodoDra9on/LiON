module.exports = 
{
	"env": 
	{
		"browser": true,
		"commonjs": true,
		"es6": true,
		"node": true
	},
	"parserOptions": 
	{
		"ecmaVersion": 2017
	},
	"extends": "eslint:recommended",
	"rules": 
	{
		"no-console": "warn",
		"eqeqeq": "error",
		"indent": ["error", "tab"],
		"quotes": [ "warn", "single" ],
		"semi": ["error", "always"],
		"comma-dangle": ["warn", "never"],
		"brace-style": ["error", "allman", { "allowSingleLine": true }]
	}
};