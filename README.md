# **L**iON: T**i**cket **O**rganizer in **N**ode.js
A complete rewrite of [Lion's Time Legacy](https://gitlab.com/chocological00/lionstime_web)!

## What it does
LiON is a web-based hallway ticket management system for students and teachers!

See the screenshots below.

SC: TBA

## How to install
Install [node](https://nodejs.org/), clone the repo, and run `npm i` inside the cloned folder.

Then run `node index.js` to start the server!

## License
Original code by [David Kim](https://gitlab.com/chocological00)

Distributed under GNU General Public License v3.0

Refer to attatched LICENSE file for details.