/* Initializing dependencies */
const express = require('express');
const route = express.Router();

module.exports = (passport) =>
{
	// google fed auth
	route.get('/google',
	    
	
			passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile'] })
	);

	route.get('/google/callback',
		passport.authenticate('google', { failureRedirect: '/', failureFlash: true}),
		(req, res) =>
		{
			// auth success
			return res.redirect('/');
		}
	);

	// logout
	route.get('/logout', (req, res) =>
	{
		req.logout();
		res.clearCookie('nyan_cat'); // destroy cookie in client
		return res.redirect('/');
	});
	
	return route;
};