/* Initializing dependencies */
const express = require('express');
const route = express.Router();

const util = require('../util');

module.exports = (settings, acc_conn, conn, fl, m, accm_conn, accfl_conn) => {
	const def = { title: settings.name };

	// pass signup page
	route.get('/signup', async (req, res) => {
		if (req.user.gradYear == 2019 || req.user.gradYear == 2022) {
			settings = await util.readSettings(fl);
		} else {
			settings = await util.readSettings(m);
		}
		if (!req.hasOwnProperty('user')) return res.redirect('/');
		if (req.user.accountLevel !== 0) return res.redirect('/'); // redirect to '/' if not a student

		const params = await util.readParams(conn); // read parameters from DB


		if (params.enabled) // if signups are opened
		{
			let variables = // variables to pass down to pug renderer
			{
				navBar: true, // show navBar
				error: req.flash('error'),
				success: req.flash('success'),
				displayname: req.user.firstName + ' ' + req.user.lastName, // user's name to display on top navbar
				gradYear: req.user.gradYear,
				purposes: settings.purposes // available purposes from settings
			};
			if (req.user.gradYear == 2019 || req.user.gradYear == 2022) {
				variables.depts = await util.getDepartments(fl); // get all the departments and save it to variables
			} else {
				variables.depts = await util.getDepartments(m);
			}

			if (util.isMobile(req.headers['user-agent'])) return res.render('mobile/pass/signup', Object.assign({}, def, variables));
			else {

				return res.render('pc/pass/signup', Object.assign({}, def, variables));
			}
		}
		else {
			req.flash('error', 'Signups are not opened!');
			return res.redirect('/');
		}
	});

	route.post('/signup', async (req, res) => {
		if (!req.hasOwnProperty('user')) return res.redirect('/');
		if (req.user.accountLevel !== 0) return res.redirect('/');

		if (req.user.gradYear == 2019 || req.user.gradYear == 2022) {
			settings = await util.readSettings(fl);
		} else {
			settings = await util.readSettings(m);
		}

		const username = req.user.username;
		const teacher = req.body.teacher;
		const purpose = req.body.purpose;
		const params = await util.readParams(conn);
		if (params.enabled) // if signups are enabled
		{
			try {
				// if pass to that teacher doesn't exist for this week (double clicking the signup button may send multiple signup requests for one teacher)
				if (req.user.gradYear == 2019 || req.user.gradYear == 2022) {
					let alreadySignedUp = ((await fl.execute('SELECT * FROM `passes` WHERE `username`=? AND `teacher`=? AND `week`=?', [username, teacher, params.week]))[0]).length > 0;
					
					let teacherFull = ((await fl.execute('SELECT * FROM `passes` WHERE `teacher`=? AND `week`=?', [teacher, params.week]))[0]).length >= parseInt((await accfl_conn.execute('SELECT `maxStudents` FROM `teachers` WHERE `username` = ?', [teacher]))[0][0].maxStudents);
					if (teacherFull) {
						req.flash('error', 'The teacher you requested is full! :(');
						return res.redirect('/');
					}
					if (!(alreadySignedUp || teacherFull)) {
						const date = new Date(); // make current date
						const dateString = date.toDateString() + ' ' + date.toTimeString(); // date string (can be parsed using date function again. refer to MDN)
						// put a pass into DB
						await fl.execute('INSERT INTO `passes` (`username`, `teacher`, `purpose`, `time`, `week`) VALUES (?, ?, ?, ?, ?)', [username, teacher, purpose, dateString, params.week]);
					}
					req.flash('success', 'Successfully signed up!');
					return res.redirect('/');
				} else {
					let alreadySignedUp = ((await m.execute('SELECT * FROM `passes` WHERE `username`=? AND `teacher`=? AND `week`=?', [username, teacher, params.week]))[0]).length > 0;
					console.log(alreadySignedUp);
					let teacherFull = ((await m.execute('SELECT * FROM `passes` WHERE `teacher`=? AND `week`=?', [teacher, params.week]))[0]).length >= parseInt((await accm_conn.execute('SELECT `maxStudents` FROM `teachers` WHERE `username` = ?', [teacher]))[0][0].maxStudents);
					if (teacherFull) {
						req.flash('error', 'The teacher you requested is full! :(');
						return res.redirect('/');
					}
					if (!(alreadySignedUp || teacherFull)) {
						const date = new Date(); // make current date
						const dateString = date.toDateString() + ' ' + date.toTimeString(); // date string (can be parsed using date function again. refer to MDN)
						// put a pass into DB

						await m.execute('INSERT INTO `passes` (`username`, `teacher`, `purpose`, `time`, `week`) VALUES (?, ?, ?, ?, ?)', [username, teacher, purpose, dateString, params.week]);

					}
					req.flash('success', 'Successfully signed up!');
					return res.redirect('/');
				}
			}
			catch (err) {
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
		else {
			req.flash('error', 'Signups are not opened!');
			return res.redirect('/');
		}
	});

	// AJAX. Grab available teachers
	route.get('/available_teachers', async (req, res) => {
		if (req.query.gradYear == 2019 || req.query.gradYear == 2022) {
			settings = await util.readSettings(fl);
		} else {
			settings = await util.readSettings(m);
		}
		if (!req.query.hasOwnProperty('dept')) return res.send(''); // if dept is undefined. DO NOT redirect as this whole page is just for AJAX purposes
		if (req.query.dept === 'Choose an option') return res.send(''); // if dept is not given send blank data
		const params = await util.readParams(conn);

		let errCount = 0;
		while (errCount < 2) {
			try {
				// SQL - select all teachers with dept (json format) and order it alphabetically
				// Compitable with MariaDB 10.2 or higher
				// Search string for json should be wrapped in ""
				if (req.query.gradYear == 2019 || req.query.gradYear == 2022) {
					let teachers = [];
					const [rows] = await accfl_conn.execute('SELECT * FROM `teachers` WHERE JSON_CONTAINS(`depts`, ?) ORDER BY `lastName` ASC', ['"' + req.query.dept + '"']);
					for (let c = 0; c < rows.length; c++) // for each rows found
					{
						const row = rows[c];
						await teachers.push([row.username, `${row.firstName} ${row.lastName}`, row.room]); // push an (array with teacher username and teacher name) in 'teacher' array
					}
					let response = '';
					for (let c = 0; c < teachers.length; c++) // for each elements in teachers array
					{
						let teacherFull = ((await fl.execute('SELECT * FROM `passes` WHERE `teacher`=? AND `week`=?', [teachers[c][0], params.week]))[0]).length >= parseInt((await accfl_conn.execute('SELECT `maxStudents` FROM `teachers` WHERE `username` = ?', [teachers[c][0]]))[0][0].maxStudents);
						response += `<option value="${teachers[c][0]}"${teacherFull ? ' disabled' : ''}>${teachers[c][1]} - ${teachers[c][2]}${teacherFull ? ' (Full)' : ''}</option>`; // value is teacher username and innerHTML is teacher name - room number

					}
					return res.send(response); // send response
				} else {
					let teachers = [];
					const [rows] = await accm_conn.execute('SELECT * FROM `teachers` WHERE JSON_CONTAINS(`depts`, ?) ORDER BY `lastName` ASC', ['"' + req.query.dept + '"']);
					for (let c = 0; c < rows.length; c++) // for each rows found
					{
						const row = rows[c];
						await teachers.push([row.username, `${row.firstName} ${row.lastName}`, row.room]); // push an (array with teacher username and teacher name) in 'teacher' array
						let response = '';
						for (let c = 0; c < teachers.length; c++) // for each elements in teachers array
						{
							let teacherFull = ((await m.execute('SELECT * FROM `passes` WHERE `teacher`=? AND `week`=?', [teachers[c][0], params.week]))[0]).length >= parseInt((await accm_conn.execute('SELECT `maxStudents` FROM `teachers` WHERE `username` = ?', [teachers[c][0]]))[0][0].maxStudents);
							response += `<option value="${teachers[c][0]}"${teacherFull ? ' disabled' : ''}>${teachers[c][1]} - ${teachers[c][2]}${teacherFull ? ' (Full)' : ''}</option>`; // value is teacher username and innerHTML is teacher name - room number
						}
						return res.send(response); // send response
					}
				}
			}
			catch (err) {
				errCount++;
			}
		}
		return res.send(''); // This is AJAX so if sth goes wrong, reasonable users would just refresh the page
	});

	// Delete pass
	route.get('/delete_pass', async (req, res) => {
		if (req.user.gradYear == 2019 || req.user.gradYear == 2022) {
			settings = await util.readSettings(fl);
		} else {
			settings = await util.readSettings(m);
		}
		if (!req.hasOwnProperty('user')) return res.redirect('/');
		if (req.user.accountLevel !== 0) return res.redirect('/'); // if not student redirect to '/'
		else {
			try {
				const passid = req.query.passid; // get pass number
				const username = req.user.username;
				if (req.user.gradYear == 2019 || req.user.gradYear == 2022) {
					await fl.execute('DELETE FROM `passes` WHERE `id` = ? AND `username` = ?', [passid, username]); // if a person tries to delete a pass that is not his/hers, nothing will happen
					req.flash('success', 'Pass Deleted!');
					return res.redirect('/');
				} else {
					await m.execute('DELETE FROM `passes` WHERE `id` = ? AND `username` = ?', [passid, username]); // if a person tries to delete a pass that is not his/hers, nothing will happen
					req.flash('success', 'Pass Deleted!');
					return res.redirect('/');
				}
			}
			catch (err) {
				req.flash('Something went wrong X(<br>Please try again!');
				return res.redirect('/');
			}
		}
	});

	return route;
};