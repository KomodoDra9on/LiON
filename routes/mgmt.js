/* Initializing dependencies */
const express = require('express');
const route = express.Router();

const util = require('../util');

module.exports = (settings, acc_conn, conn) =>
{
	/* Initializing general variables */
	// Default variable set for layout.pug
	const def = { title: settings.name + ' Management Panel' };
	
	/* Routing */
	route.get('/test_other_view', async (req, res) => 
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');

		let userObj = JSON.parse(req.session.passport.user);
		userObj[1] = req.user.realIdentity;
		req.session.passport.user = JSON.stringify(userObj);

		return res.redirect('/');
	});

	// adding one user
	route.get('/add_user', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/'); // if not logged in
		if(req.user.accountLevel !== 2) return res.redirect('/'); // if not admin, redirect
		
		const depts = await util.getDepartments(conn);
		let variables =
			{
				username: req.user.username,
				displayname: req.user.firstName + ' ' + req.user.lastName,
				navBar: true,
				error: req.flash('error'),
				success: req.flash('success'),
				depts: JSON.stringify(depts)
			};
		
		return res.render('pc/mgmt/add_user', Object.assign({}, def, variables));
	});
	
	route.post('/add_user', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');
		
		if(!(req.body.hasOwnProperty('accountLevel') && req.body.hasOwnProperty('username')))
		{
			req.flash('error', 'Some fields are blank!');
			return res.redirect('/mgmt/add_user');
		}
		const accountLevel = parseInt(req.body.accountLevel);
		const username = req.body.username.trim();
		let firstName;
		if(req.body.hasOwnProperty('firstName')) firstName = req.body.firstName.trim();
		let lastName;
		if(req.body.hasOwnProperty('lastName')) lastName = req.body.lastName.trim();
		let gradYear;
		if(req.body.hasOwnProperty('gradYear')) gradYear = req.body.gradYear; // student only
		let depts;
		if(req.body.hasOwnProperty('depts')) depts = req.body.depts; // teachers only
		let maxStudents;
		if(req.body.hasOwnProperty('maxStudents')) maxStudents = req.body.maxStudents; // teachers only
		let studentOrTeacher;
		if(req.body.hasOwnProperty('studentOrTeacher')) studentOrTeacher = req.body.studentOrTeacher;

		switch(accountLevel) 
		{
		case 0:
		{
			// check if any fields are blank
			if (!(username && firstName && lastName && gradYear))
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/mgmt/add_user');
			}
			try
			{
				// prepared statements - automatically escapes certain characters to prevent SQL injection
				const [rows] = await acc_conn.execute('SELECT * FROM `students` WHERE `username`=?', [username]); // search for that username first
				if (rows.length > 0) // if that username exists redirect with error
				{
					req.flash('error', 'Username already exists in students\' database!');
					return res.redirect('/mgmt/add_user');
				}
				else
				{
					// add that user
					await acc_conn.execute('INSERT INTO `students` (`username`, `firstName`, `lastName`, `gradYear`) VALUES ' +
						'(?, ?, ?, ?)', [username, firstName, lastName, gradYear]);
					// redirect with success
					req.flash('success', `Student ${username} added successfully!`);
					return res.redirect('/mgmt/add_user');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
			
		case 1:
		{
			if (!(username && firstName && lastName && maxStudents && depts))
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/mgmt/add_user');
			}
			
			try
			{
				let departments = [];
				for(let c = 0; c < depts.length; c++)
				{
					if(!depts[c].startsWith('Department'))
					{
						let alreadyExists = false;
						for(let i = 0; i < departments.length; i++)
						{
							if(departments[i] === depts[c]) alreadyExists = true;
						}
						if(!alreadyExists) departments.push(depts[c]);
					}
				}

				const [rows] = await acc_conn.execute('SELECT * FROM `teachers` WHERE `username`=?', [username]);

				if (rows.length > 0) // if that username exists redirect with error
				{
					req.flash('error', 'Username already exists in teachers\' database!');
					return res.redirect('/mgmt/add_user');
				}
				else
				{
					await acc_conn.execute('INSERT INTO `teachers` (`username`, `firstName`, `lastName`, `depts`, `maxStudents`) VALUES ' +
						'(?, ?, ?, ?, ?)', [username, firstName, lastName, JSON.stringify(departments), maxStudents]);
					// redirect with success
					req.flash('success', `Room account ${username} added successfully!`);
					return res.redirect('/mgmt/add_user');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
			
		case 2:
		{
			if (!(username && studentOrTeacher))
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/mgmt/add_user');
			}
			
			try
			{
				studentOrTeacher = parseInt(studentOrTeacher);
				if(studentOrTeacher === 0)
				{
					const [rows] = await acc_conn.execute('SELECT * FROM `students` WHERE `username`=?', [username]);

					if(rows.length === 0)
					{
						req.flash('error', 'User not found in Student\'s database!');
						return res.redirect('/mgmt/add_user');
					}
					else
					{
						await acc_conn.execute('UPDATE `students` SET `accountLevel` = ? WHERE `username` = ?', [2, username]);
						req.flash('success', `Account ${username} promoted successfully!`);
						return res.redirect('/mgmt/add_user');
					}
				}
				else if(studentOrTeacher === 1)
				{
					const [rows] = await acc_conn.execute('SELECT * FROM `teachers` WHERE `username`=?', [username]);

					if(rows.length === 0)
					{
						req.flash('error', 'User not found in teacher\'s database!');
						return res.redirect('/mgmt/add_user');
					}
					else
					{
						await acc_conn.execute('UPDATE `teachers` SET `accountLevel` = ? WHERE `username` = ?', [2, username]);
						req.flash('success', `Account ${username} promoted successfully!`);
						return res.redirect('/mgmt/add_user');
					}
				}
				else 
				{
					req.flash('error', '500 Internal Server Error');
					return res.redirect('/mgmt/add_user');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}

		default:
			req.flash('error', '500 Internal Server Error');
			return res.redirect('/mgmt/add_user');
		}
		
	});
	
	// adding multiple users
	route.get('/add_users', (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');
		
		let variables =
			{
				username: req.user.username,
				displayname: req.user.firstName + ' ' + req.user.lastName,
				navBar: true,
				error: req.flash('error'),
				success: req.flash('success')
			};
		return res.render('pc/mgmt/add_users', Object.assign({}, def, variables));
	});
	
	route.post('/add_users', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');
		
		let field = req.body.field;

		if(!field)
		{
			req.flash('error', 'The field is blank!');
			return res.redirect('/mgmt/add_users');
		}
		
		let lines = await field.split('\n'); // split by line breaks and make an array
		for(let c = 0; c < lines.length; c++) // for each lines
		{
			lines[c] = await lines[c].split(','); // split by comma and make an array
			for(let i = 0; i < lines[c].length; i++) // for each elements
			{
				lines[c][i] = await lines[c][i].trim(); // lines is now a 2D array. inner array contains one line information separated by comma
			}
		}
		
		let errCount = 0; // counter for parse error - does not handle DB error
		for(let c = 0; c < lines.length; c++) // for each lines
		{
			let line = lines[c];
			
			let username = line[0];
			let lastName = line[1];
			let firstName = line[2];
			let gradYear = line[3];
			
			if(username&&lastName&&firstName&&gradYear) // if parsed correctly
			{
				// for concurrent execution
				(async () => 
				{
					try
					{
						acc_conn.execute('INSERT INTO `students` (`username`, `firstName`, `lastName`, `gradYear`) VALUES (?, ?, ?, ?)',
							[username, firstName, lastName, gradYear]);
					}
					catch(err)
					{
						errCount++; // DB is executed concurrently, so I don't think this will do anything tbh
					}
				})();
			}
			else errCount++; // if not parsed correctly increase the count
		}

		if(errCount) // if error exists
		{
			req.flash('error', `Error parsing ${errCount} line(s). Check the database manually.`);
			return res.redirect('/mgmt/add_users');
		}
		else
		{
			req.flash('error', 'Processing started! Please allow some time for the server to finish adding accounts!');
			return res.redirect('/mgmt/add_users');
		}
	});
	
	// Change website settings
	route.get('/settings', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');
		
		try
		{
			let variables =
			{
				username: req.user.username,
				displayname: req.user.firstName + ' ' + req.user.lastName,
				navBar: true,
				error: req.flash('error'),
				success: req.flash('success')
			};
			variables.rows = (await conn.execute('SELECT * FROM `settings`'))[0];
			return res.render('pc/mgmt/settings', Object.assign({}, def, variables));
		}
		catch(err)
		{
			req.flash('error', '500 Internal Server Error X(');
			return res.redirect('/');
		}
	});
	
	route.post('/settings', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');
		for(let key in req.body)
		{
			try
			{
				if(key === 'submit') continue; // skip the submit button
				await conn.execute('UPDATE `settings` SET `value` = ? WHERE `key` = ?', [req.body[key], key]); // update settings
			}
			catch(err)
			{
				req.flash('error', '500 Internal server Error X(');
				return res.redirect('/');
			}
		}
		req.flash('success', 'Settings successfully updated! The website will be restarted in 10 seconds...');
		res.redirect('/mgmt/settings'); // no return here - the code below has to run
		setTimeout(() => { process.exit(); }, 10000); // process MUST be restarted from external task manager (sth like pm2 or forever)
	});
	
	// statistics page
	route.get('/stats', (req, res) =>
	{
		// TODO: write stats page
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');
		return res.render('pc/mgmt/stats', Object.assign({}, def, {}));
	});
	
	// change signup open status
	route.get('/change_status', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');

		let variables =
			{
				username: req.user.username,
				displayname: req.user.firstName + ' ' + req.user.lastName,
				navBar: true,
				error: req.flash('error'),
				success: req.flash('success'),
				weekday: settings.weekday
			};
		const params = await util.readParams(conn);
		params.enabled ? variables.status = 'enabled' : variables.status = 'disabled'; // ternary operator. sets current status in string
		return res.render('pc/mgmt/change_status', Object.assign({}, def, variables));
	});
	
	route.post('/change_status', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if(req.user.accountLevel !== 2) return res.redirect('/');
		
		if(req.body.change === 'change')
		{
			const params = await util.readParams(conn);
			try
			{
				if(params.enabled) // if current status is enabled
				{
					await conn.execute('UPDATE `params` SET `value` = ? WHERE `key` = ?', ['false', 'enabled']);
					req.flash('success', `Signups are closed until closest ${settings.weekday}!`);
					return res.redirect('/mgmt/change_status');
				}
				else
				{
					await conn.execute('UPDATE `params` SET `value` = ? WHERE `key` = ?', ['true', 'enabled']);
					req.flash('success', `Signups are opened until closest day before ${settings.weekday}!`);
					return res.redirect('/mgmt/change_status');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
		else
		{
			req.flash('error', 'Internal Server Error!');
			return res.redirect('/mgmt/change_status');
		}
	});
	
	return route;
};