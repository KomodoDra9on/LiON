/**
 * reads settings table from DB
 * @param conn - connection object to main DB
 * @returns {Promise<Object>} - use await
 */
exports.readSettings = async (conn) =>
{
	let errCount = 0;
	while(errCount < 2)
	{
		try
		{
			let settings = {};
			const rows = (await conn.execute('SELECT * FROM `settings`'))[0];
			for(let c = 0; c < rows.length; c++)
			{
				let row = rows[c];
				settings[row.key] = row.value; // assigns settings.{key} to {value}
			}
			
			// Parse purposes (in JSON format)
			// purposes are saved in DB as a JSON format so parse it
			settings.purposes = await JSON.parse(settings.purposes);
			
			// add a separate 'weekday' - string representation of day (which is in 0-6)
			settings.weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][settings.day];
			
			return settings;
		}
		catch(err)
		{
			errCount++;
		}
	}
	if(errCount >= 2) return null; // should I just kill the process here? (so it can be restarted by external process manager)
};

/**
 * reads params table from DB
 * @param conn - connection object to main DB
 * @returns {Promise<Object>} - use await
 */
exports.readParams = async (conn) =>
{
	let errCount = 0;
	while(errCount < 2)
	{
		try
		{
			let params = {};
			const rows = (await conn.execute('SELECT * FROM `params`'))[0];
			for(let c = 0; c < rows.length; c++)
			{
				let row = rows[c];
				params[row.key] = row.value;
			}
			
			// parse enabled
			// enabled is saved in DB as a string format so parse it to boolean
			if(params.enabled === 'false') params.enabled = false;
			else if (params.enabled === 'true') params.enabled = true;
			
			return params;
		}
		catch(err)
		{
			errCount++;
		}
	}
	if(errCount >= 2) process.exit();
};

/**
 * get list of departments from DB
 * @param acc_conn - connection object to account DB
 * @returns {Promise<Array>} - string array of list of departments
 */
exports.getDepartments = async (conn) =>
{
	let errCount = 0;
	while(errCount < 2)
	{
		try
		{
			const [[row]] = await conn.execute('SELECT `value` FROM `settings` WHERE `key` = ?', ['depts']);
			return await JSON.parse(row.value);
		}
		catch(err)
		{
			errCount++;
		}
	}
	if(errCount >= 2) return null;
};

exports.isMobile = (UA) =>
{
	const MD = new require('mobile-detect');
	let md = new MD(UA);
	// eslint-disable-next-line eqeqeq
	if((md.mobile()) && !(md.tablet())) return true;
	else return false;
};