// TODO: wrap every single DB call with Try/Catch
// TODO: make separate mobile view
// NOTE: for the routes, field and user level errors are handled by 'return res.redirect()' to reduce tabs
//       DB related errors are still handled by if else

// Initializing Dependencies

// Filesystem access
const fs = require('fs');

// express web framework
const express = require('express');
const app = express();
app.disable('x-powered-by'); // hide server software information from response header - better for security

// express middlewares

// POST parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));

// Pug template engine
app.locals.pretty = true; // dev setting. disable when in production environment
app.set('views', './views');
app.set('view engine', 'pug');

// MariaDB connector
const mysql = require('mysql2/promise');

// MariaDB Session Store
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);

// flash session store - handles error msg
const flash = require('connect-flash');
app.use(flash());

// Passportjs Authentication Library
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

// CronJob
const CronJob = require('cron').CronJob;

// util file
const util = require('./util');

// main function
async function main()
{
	// Read file data
	process.stdout.write('Reading Credentials... ');
	const SQL = await JSON.parse(fs.readFileSync('data/SQL.json', 'utf8'));
	const DATA = await JSON.parse(fs.readFileSync('data/DATA.json', 'utf8'));
	const WTF = await JSON.parse(fs.readFileSync('data/WTF.json', 'utf8'));
	// eslint-disable-next-line no-console
	console.log('Done!');
	
	// make DB connection
	process.stdout.write('Connecting to Database... ');
	// Website database
	const conn = await mysql.createConnection(
		{
			host: SQL.host,
			user: SQL.user,
			password: SQL.password,
			database: SQL.database
		});
	conn.connect();
	const acc_conn = await mysql.createConnection(
		{
			host: SQL.host,
			user: SQL.user,
			password: SQL.password,
			database: SQL.acc_database
		});
			const fl = await mysql.createConnection(
		{
			host: WTF.host,
			user: WTF.user,
			password: WTF.password,
			database: WTF.fl
		});
	fl.connect();
	const accfl_conn = await mysql.createConnection(
		{
			host: WTF.host,
			user: WTF.user,
			password: WTF.password,
			database: WTF.flteachers
		});
			const m = await mysql.createConnection(
		{
			host: WTF.host,
			user: WTF.user,
			password: WTF.password,
			database: WTF.m
		});
	m.connect();
	const accm_conn = await mysql.createConnection(
		{
			host: WTF.host,
			user: WTF.user,
			password: WTF.password,
			database: WTF.mteachers
		});	
	// eslint-disable-next-line no-console
	console.log('Done!');
	
	// express session with MariaDB store
	process.stdout.write('Initializing Session Storage... ');
	const sessionStore = new MySQLStore({}, acc_conn);
	// initializing express session
	app.use(session(
		{
			key: 'nyan_cat',
			secret: DATA.session.secret, // random string to encrypt the cookie value
			store: sessionStore,
			resave: false,
			saveUninitialized: true,
			maxAge: 2592000000, // login status kept
			cookie:
				{
					maxAge: 2592000000 // cookie kept
				}
		}));
	// eslint-disable-next-line no-console
	console.log('Done!');

	// initialize passportjs
	app.use(passport.initialize());
	app.use(passport.session());
	passport.use(new GoogleStrategy(
		{
			clientID: DATA.google.clientID,
			clientSecret: DATA.google.clientSecret,
			callbackURL: '/auth/google/callback'
		},
		async (accessToken, refreshToken, profile, cb) => 
		{
			const domain = profile._json.hd; // google account domain for managed user accounts

			if(domain === 'hcpss.org') // if user is teacher
			{
							try
				{
					const googleID = profile.id; // google provided UUID
					const [[user]] = await acc_conn.execute('SELECT * FROM `teachers` WHERE `googleID` = ?', [googleID]);
					if(user) return cb(null, user); 
					else // if it's user's first time logging in with google
					{
						const googleEmail = profile.emails[0].value; // 1st email associated with that google account
						const [username] = googleEmail.split('@', 1); // username retrived from email
						const [[user]] = await acc_conn.execute('SELECT * FROM `teachers` WHERE LCASE(username) LIKE LCASE(?)', [username]); // get user with the matching username
						if(user) // if user with the matching username is found
						{
							const firstName = profile.name.givenName; // first name retrived from google
							const lastName = profile.name.familyName; // last name retrived from google
							await acc_conn.execute('UPDATE `teachers` SET `googleID` = ?, `firstName` = ?, `lastName` = ?, `email` = ? WHERE `username` = ?',
								[googleID, firstName, lastName, googleEmail, username]); // update DB with info reterived from Google
							const [[user]] = await acc_conn.execute('SELECT * FROM `teachers` WHERE `googleId` = ?', [googleID]); // find the user just added
							return cb(null, user); // end login procedure
						}
						else return cb(null, false, { message: 'It seems like you are not a registered as a Howard Teacher!<br>Are you new? Please contact to Mr.Zaron @ A200 to claim your account!' });
					}
				}
				catch(err)
				{
					return cb(null, false, { message: '500 Internal Server Error X(<br>Please Retry!'});
				}
			}

			else if(domain === 'inst.hcpss.org') // if user is student
			{
				try
				{
					const googleID = profile.id; // user's google UUID
					const [[user]] = await acc_conn.execute('SELECT * FROM `students` WHERE `googleID` = ?', [googleID]); // get matching user with the UUID if found in the DB
					if(user) return cb(null, user); // if matching user exists end the statement
					else // if it's user's first time logging in with google
					{
						const googleEmail = profile.emails[0].value; // 1st email associated with that google account
						const [username] = googleEmail.split('@', 1); // username retrived from email
						const [[user]] = await acc_conn.execute('SELECT * FROM `students` WHERE `username` = ?', [username]); // get user with the matching username
						if(user) // if user with the matching username is found
						{
							const firstName = profile.name.givenName; // first name retrived from google
							const lastName = profile.name.familyName; // last name retrived from google
							await acc_conn.execute('UPDATE `students` SET `googleID` = ?, `firstName` = ?, `lastName` = ?, `email` = ? WHERE `username` = ?',
								[googleID, firstName, lastName, googleEmail, username]); // update DB with info reterived from Google
							const [[user]] = await acc_conn.execute('SELECT * FROM `students` WHERE `googleId` = ?', [googleID]); // find the user just added
							return cb(null, user); // end login procedure
						}
						else return cb(null, false, { message: 'It seems like you are not a Howard student!<br>Are you new? Please contact to Mr.Zaron @ A200 to claim your account!' });
					}
				}
				catch(err)
				{
					return cb(null, false, { message: '500 Internal Server Error X(<br>Please Retry!'});
				}
			}

			else return cb(null, false, { message: 'It seems like the account you provided is not linked to HCPSS!<br>Are you using HCPSS provided Google Account?<br>Error Code: 401' });
		}));
	passport.serializeUser((user, done) =>
	{
		return done(null, user.googleID);
	});
	passport.deserializeUser(async (googleID, done) =>
	{
		try
		{
			let [row] = await acc_conn.execute('SELECT * FROM `students` WHERE `googleID` = ?', [googleID]);
			if(row.length === 0) [row] = await acc_conn.execute('SELECT * FROM `teachers` WHERE `googleID` = ?', [googleID]);
			return done(null, row[0]);
		}
		catch(err)
		{
			return done(err);
		}
	});
	
	// Initializing General Variables
	
	// port
	const port = 8888;
	
	// Read settings from DB
	// settings is read as a general variable to reduce DB call (as they don't change that often anyways)
	process.stdout.write('Reading Settings from Database... ');
	let settings = await util.readSettings(conn);
	// await - the function calls DB inside
	// eslint-disable-next-line no-console
	console.log('Done!');
	
	// Routing
	// Robots.txt - prevents search engine crawling
	// Google it for more information
	app.get('/robots.txt', (req, res) =>
	{
		res.type('text/plain'); // serving in text file format
		res.send('User-agent: *\n' +
			'Disallow: /auth/\n' +
			'Disallow: /mgmt/\n');
	});
	
	// Dynamic Routes
	const auth = require('./routes/auth')(passport); // passing down connections
	app.use('/auth', auth);
	const mgmt = require('./routes/mgmt')(settings, acc_conn, conn);
	app.use('/mgmt', mgmt);
	const pass = require('./routes/pass')(settings, acc_conn, conn, fl, m, accm_conn, accfl_conn);
	app.use('/pass', pass);
	const root = require('./routes/root')(settings, acc_conn, conn, fl, m, accm_conn, accfl_conn); // root should always go on the bottom
	app.use('/', root);
	
	// Static Routes
	// serving everything under public folder
	// css, js, you name it!
	app.use(express.static('public'));

	// handling 404
	app.get('*', (req, res) =>
	{
		res.status(404);
		res.sendFile('public/img/404.png', { root: __dirname });
	});
	
	// Starting Server
	app.listen(port, () =>
	{
		// eslint-disable-next-line no-console
		console.log(`The server is ready! Running on http://localhost:${port}`);
	});
	
	// Cron Jobs
	// Note: refer to the documentation on the npmjs.com
	// This cronjob module do not support weekday 7 - use 0
	// on 14:10 the day before
	// to keep the DB connection alive (every hour)
		(new CronJob('0 0 * * * *', async () =>
	{
		let errCount = 0;
		while(errCount < 10)
		{
			try
			{
				await conn.execute('SELECT 1');
				await acc_conn.execute('SELECT 1');
				await accm_conn.execute('SELECT 1');
				await accmfl_conn.execute('SELECT 1');
				await m_conn.execute('SELECT 1');
				await flm_conn.execute('SELECT 1');
				break;
			}
			catch(err)
			{
				errCount++;
			}
		}
	})).start();
		// TODO: email error to admin if errCount exceeds 10

	// on 14:10 the day
	new CronJob(`0 ${settings.schoolEndMins} ${settings.schoolEndHrs} * * ${settings.day} *`, async () =>
	{
		let errCount = 0;
		while(errCount < 10)
		{
			try
			{
				await conn.execute('UPDATE `params` SET `value`=? WHERE `key`=?', ['true', 'enabled']); // set enabled to string 'true'
			}
			catch(err)
			{
				errCount++;
			}
		}
		
		try
		{
			const params = await util.readParams(conn);
			conn.execute('UPDATE `params` SET `value`=? WHERE `key`=?', [parseInt(params.week) + 1, 'week']); // add one to 'week'
		}
		catch(err)
		{
			// TODO: email error to admin (we don't want the repeat method to increase the week value by 2 or more)
		}
	});
}

main(); // run the main function